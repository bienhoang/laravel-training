<?php

include "PrintAnother.php";

class UseTrait
{
	use PrintAnother;

	public function print($type = null)
	{
		if ($type) {
			switch ($type) {
				case 'PDF':
					$this->printPDF();
					break;
				case 'WORD':
					$this->printWord();
					break;
				case 'EXCEL':
					$this->printExcel();
					break;
				default:
					break;
			}
		} else {
			echo "Please insert type for printer";
		}
	}
}

$printer = new UseTrait();
$printer->print('PDF');
$printer->print('WORD');
$printer->print('EXCEL');