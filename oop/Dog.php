<?php

include "Runnable.php";

class Dog implements Runnable
{
	public $name;
	public $color;
	public $gender;
	public $type;

	private function sua()
	{
		echo "Gau gau";
	}

	public function run()
	{
		return "Dog is Running";
	}

	public function play()
	{
		echo "Playing";
	}

	public function choPhepSua()
	{
		$this->sua();
	}
}
