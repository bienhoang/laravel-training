<?php

class FinalClass
{
	public final function eat()
	{
		echo "Eating";
	}
}

class NormalClass extends FinalClass
{

}

$nl = new NormalClass();
$nl->eat();