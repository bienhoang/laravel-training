<?php

trait PrintAnother
{
	function printPDF()
	{
		echo "Printing PDF \n";
	}

	function printWord()
	{
		echo "Printing Word \n";
	}

	function printExcel()
	{
		echo "Printing Excel \n";
	}
}
