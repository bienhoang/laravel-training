<?php

class StaticClass
{
	public static $name = "Bien Hoang";
	public $gender;

	public function run()
	{
		echo "Running";
	}

	public static function eat()
	{
		echo "Eating";
	}
}

$object = new StaticClass();
echo $object->name;
$object->eat();

//echo StaticClass::$name;
//StaticClass::eat();