<?php

include "Dog.php";
include "Sanho.php";

class Store
{
	public function cook(Runnable $animal)
	{
		echo "Cooking: " . $animal->run();
	}
}

$dog = new Dog();

$store = new Store();

$store->cook($dog);

$sanho = new Sanho();

$store->cook($sanho);