<?php

include "MyClass.php";
include "../oop2/MyClass.php";

class YourClass
{
	public function __construct()
	{
		$class = new \oop\MyClass();
		$class->printClass();

		$class2 = new \oop2\MyClass();
		$class2->printClass();
	}
}

$yourClass = new YourClass();