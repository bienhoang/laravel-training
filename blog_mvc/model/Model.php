<?php

class Model
{
	/** @var string */
	protected $table;

	/** @var PDO */
	public $db;

	/**
	 * Model constructor.
	 */
	public function __construct()
	{
		$this->db = include "../config.php";
		$this->db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	}

	/**
	 * Read by ID
	 *
	 * @param int|string $id
	 * @return array
	 */
	public function read($id)
	{
		$stmt = $this->db->prepare("SELECT * FROM $this->table WHERE id=:id");
		$stmt->bindParam(':id', $id);
		$stmt->execute();
		return $stmt->fetch(PDO::FETCH_ASSOC);
	}

	/**
	 * Delete by ID
	 *
	 * @param int|string $id
	 */
	public function delete($id)
	{
		$stmt = $this->db->prepare("DELETE * FROM $this->table WHERE id=:id");
		$stmt->bindParam(':id', $id);
		$stmt->execute();
	}
}
