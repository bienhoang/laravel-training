<?php

include "../config.php";

class Comment
{
	public $table = 'comments';

	public $id;
	public $post_id;
	public $content;
	public $name;
	public $email;
	public $website;
	public $approved;
	public $created_at;
	public $updated_at;

	public function read()
	{

	}

	public function update()
	{

	}

	public function delete()
	{

	}

	public function save()
	{

	}

}