<?php
include "includes/header.php";
include "includes/sidebar.php";

if (!isset($_SESSION['admin'])) {
	header('Location: login.php');
	exit;
}

$db = include "db.php";

// Get categories list
$stmt = $db->prepare("SELECT * FROM categories");
$stmt->execute();
$categories = $stmt->fetchAll(PDO::FETCH_ASSOC);

// Get post
$postId = $_GET['id'];
$stmt = $db->prepare("SELECT * FROM posts WHERE id=:id");
$stmt->bindParam(':id', $postId);
$stmt->execute();
$post = $stmt->fetch(PDO::FETCH_ASSOC);

if (empty($post)) {
	header('Location: list_post.php');
}

// Get list tag
$tag = unserialize($post['tag_id']);
$tag = implode(',', $tag);
$stmt = $db->prepare("SELECT name FROM tags WHERE id IN(" . $tag . ")");
$stmt->execute();

$tagList = $stmt->fetchAll(PDO::FETCH_ASSOC);

foreach ($tagList as $tag) {
	$post['tag'][] = $tag['name'];
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	$title = $_POST['title'];
	$content = $_POST['content'];
	$category = $_POST['category'];
	$tagList = $_POST['tag'];

	if (empty($title)) {
		$error['title'] = 'Title is required';
	}

	if (empty($content)) {
		$error['content'] = 'Content is required';
	}

	if (empty($category)) {
		$error['category'] = 'Category is required';
	}

	$tags = explode(',', $tagList);

	if (empty($tags)) {
		$tags[] = 'all';
	}

	$tagId = [];

	foreach ($tags as $tag) {
		// Check tag exist
		$stmt = $db->prepare("SELECT * FROM tags WHERE name = :name");
		$stmt->bindParam(':name', trim($tag));
		$stmt->execute();
		$existTag = $stmt->fetch(PDO::FETCH_ASSOC);

		if ($existTag) {
			$tagId[] = $existTag['id'];
		} else {
			// If not exist then create new tag
			$stmt = $db->prepare("INSERT INTO tags(name) VALUES (:name)");
			$stmt->bindParam(':name', $tag);
			$stmt->execute();
			$tagId[] = $db->lastInsertId();
		}
	}

	if (empty($error)) {
		$stmt = $db->prepare("UPDATE posts SET title=:title, content=:content, category_id=:category_id, tag_id=:tag_id");
		$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
		$stmt->bindParam(':title', $title);
		$stmt->bindParam(':content', $content);
		$stmt->bindParam(':category_id', $category);
		$stmt->bindParam(':tag_id', serialize($tagId));

		$stmt->execute();
		header('Location: list_post.php');
	}
}
?>
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Add Post</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <form role="form" action="" method="POST">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input class="form-control" type="text" name="title" id="title" value="<?=$post['title'] ?>" />
						<?php if (isset($error['title'])) echo $error['title'] ;?>
                    </div>
                    <div class="form-group">
                        <label for="content">Content</label>
                        <textarea class="form-control" rows="3" name="content" id="content" ><?=$post['content'] ?></textarea>
						<?php if (isset($error['content'])) echo $error['content'] ;?>
                    </div>
                    <div class="form-group">
                        <label for="category">Category</label>
                        <select class="form-control" id="category" name="category">
                            <option></option>
							<?php foreach ($categories as $cat):?>
                                <?php if ($post['category_id'] == $cat['id']):?>
                                <option value="<?=$cat['id'] ?>" selected><?=$cat['name'] ?></option>
                                <?php else: ?>
                                <option value="<?=$cat['id'] ?>"><?=$cat['name'] ?></option>
                                <?php endif; ?>
							<?php endforeach; ?>
                        </select>
						<?php if (isset($error['category'])) echo $error['category'] ;?>
                    </div>
                    <div class="form-group">
                        <label>Tags</label>
                        <input class="form-control" type="text" placeholder="Nhập cách nhau bằng dấu phẩy" name="tag" id="tag"
                            value="<?php echo implode(',', $post['tag']) ?>" />
                    </div>
                    <button type="submit" class="btn btn-success">Update</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                </form>
            </div>
        </div>
    </div>
<?php include "includes/footer.php"; ?>