<?php
include "includes/header.php";
include "includes/sidebar.php";
if (!isset($_SESSION['admin'])) {
	header('Location: login.php');
	exit;
}
$db = include "db.php";

$stmt = $db->prepare("SELECT * FROM categories");
$stmt->execute();
$categories = $stmt->fetchAll(PDO::FETCH_ASSOC);


if (isset($_GET['delete'])) {
    $stmt = $db->prepare("DELETE FROM categories WHERE id=:id");
    $stmt->bindParam(':id', $_GET['delete']);
    $stmt->execute();

    header('Location: list_category.php');
}

?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">List Categories</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Blog Category
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="80%">Name</th>
                                <th width="5%">Edit</th>
                                <th width="5%">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($categories as $cat) :?>
                            <tr>
                                <td><?=$cat['id']?></td>
                                <td><?=$cat['name']?></td>
                                <td><a href="update_category.php?id=<?=$cat['id']?>" class="btn btn-primary">Edit</a></td>
                                <td><a href="list_category.php?delete=<?=$cat['id']?> " class="btn btn-danger">Delete</a></td>
                            </tr>
                            <?php endforeach;?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <a href="add_category.php" class="btn btn-warning">Add Category</a>
    </div>
</div>
<?php include "includes/footer.php"; ?>