<?php
include "includes/header.php";
if (!isset($_SESSION['admin'])) {
	header('Location: login.php');
	exit;
}
include "includes/sidebar.php";
include "includes/footer.php";
