<?php
include "includes/header.php";
include "includes/sidebar.php";
if (!isset($_SESSION['admin'])) {
	header('Location: login.php');
	exit;
}
$db = include "db.php";

$stmt = $db->prepare("SELECT posts.*, categories.name as cat_name FROM posts INNER JOIN categories ON posts.category_id = categories.id");
$stmt->execute();
$posts = $stmt->fetchAll(PDO::FETCH_ASSOC);

foreach ($posts as $key => $post) {
    // Get tag list
    $tag = unserialize($post['tag_id']);
    $tag = implode(',', $tag);
	$stmt = $db->prepare("SELECT * FROM tags WHERE id IN(" . $tag . ")");
	$stmt->execute();

	$tagList = $stmt->fetchAll(PDO::FETCH_ASSOC);
	$posts[$key]['tag_id'] = $tagList;
}

if (isset($_GET['delete'])) {
	$stmt = $db->prepare("DELETE FROM posts WHERE id=:id");
	$stmt->bindParam(':id', $_GET['delete']);
	$stmt->execute();

	header('Location: list_post.php');
}
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">List Posts</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Blog Post
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="25%">Title</th>
                                <th width="25%">Summary</th>
                                <th width="10%">Category</th>
                                <th width="15%">Tag</th>
                                <th width="10%">User</th>
                                <th width="5%">Edit</th>
                                <th width="5%">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($posts as $post): ?>
                            <tr>
                                <td><?=$post['id'] ?></td>
                                <td><?=$post['title'] ?></td>
                                <td><?=$post['content'] ?></td>
                                <td><?=$post['cat_name']?></td>
                                <td>
                                <?php foreach ($post['tag_id'] as $tag):?>
                                    <a href="/tags?tag=<?=trim($tag['name']);?>"><span class="btn btn-outline btn-info"><?=trim($tag['name']);?></span></a>
                                <?php endforeach; ?>
                                </td>
                                <td>bienhoang</td>
                                <td><a href="update_post.php?id=<?=$post['id']?>" class="btn btn-primary">Edit</a></td>
                                <td><a href="list_post.php?delete=<?=$post['id']?>" class="btn btn-danger">Delete</a></td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <a href="add_post.php" class="btn btn-warning">Add Post</a>
    </div>
</div>
<?php include "includes/footer.php"; ?>