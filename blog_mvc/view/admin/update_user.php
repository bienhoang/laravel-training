<?php
	include "includes/header.php";
	include "includes/sidebar.php";
    if (!isset($_SESSION['admin'])) {
        header('Location: login.php');
        exit;
    }
	$db = include "db.php";
    $error = [];

    $id = $_GET['id'];
    $stmt = $db->prepare("SELECT * FROM users WHERE id = :id");
    $stmt->bindParam(':id', $id);
    $stmt->execute();

    $user = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$user) {
		header('Location: list_user.php');
    }

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	    $password = $_POST['password'];
	    $email = $_POST['email'];
	    $role = $_POST['role'];

		if (empty($email)) {
			$error['email'] = 'Email is required';
		}

        // Nếu không có lỗi gì thì tiến hành xử lý để lưu vào DB.
		if (empty($error)) {
		    $query = "UPDATE users SET";

			if (md5($password) != $user['password']) {
				$query .= " password='" . md5($password) . "'";
			}

			if ($email != $user['email']) {
				$query .= ", email='" . $email . "'";
			}

			if ($role != $user['role'])
			{
				$query .= ", role=" . $role;
			}

			$query .= ' WHERE id=' . $id;

			$stmt = $db->prepare($query);
			$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

			$stmt->execute();

//			header('Location: list_user.php');
        }
    }

?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Update User</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form role="form" action="" method="POST">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input class="form-control" type="text" name="username" id="username" value="<?php if (isset($user['username'])) echo $user['username']; ?>" disabled/>
                    <?php if (isset($error['username'])) echo $error['username'] ;?>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input class="form-control" type="password" name="password" id="password">
                    <?php if (isset($error['password'])) echo $error['password'] ;?>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input class="form-control" type="text" name="email" id="email" value="<?php if (isset($user['email'])) echo $user['email']; ?>">
                    <?php if (isset($error['email'])) echo $error['email'] ;?>
                </div>
                <div class="form-group">
                    <label for="role">Role</label>
                    <select class="form-control" id="role" name="role">
                        <option></option>
                        <option value="1" <?php if ($user['role'] == 1) echo 'selected'; ?>>1</option>
                        <option value="2" <?php if ($user['role'] == 2) echo 'selected'; ?>>2</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-success">Update</button>
                <a href="list_user.php" class="btn btn-danger">Back</a>
            </form>
        </div>
    </div>
</div>
<?php include "includes/footer.php"; ?>