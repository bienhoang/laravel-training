<?php
	include "includes/header.php";
	include "includes/sidebar.php";
    if (!isset($_SESSION['admin'])) {
        header('Location: login.php');
        exit;
    }
	$db = include "db.php";
    $error = [];

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	    $username = $_POST['username'];
	    $password = $_POST['password'];
	    $email = $_POST['email'];
	    $role = $_POST['role'];

	    // Kiểm tra xem các dữ liệu gửi lên có rỗng không. Nếu rỗng thì trả về lỗi.
	    if (empty($username)) {
	        $error['username'] = 'Username is required';
        }

		if (empty($password)) {
			$error['password'] = 'Password is required';
		}

		if (empty($email)) {
			$error['email'] = 'Email is required';
		}

		// Kiểm tra xem username đã tồn tại trong DB chưa, nếu tồn tại thì trả về lỗi.
		$stmt = $db->prepare("SELECT id FROM users WHERE username = :username");
		$stmt->bindParam(':username', $username);
		$stmt->execute();

		$result = $stmt->fetch();

		if ($result) {
		    $error['username'] = 'Username existed, please choice another one';
        }

        // Nếu không có lỗi gì thì tiến hành xử lý để lưu vào DB.
		if (empty($error)) {
			$stmt = $db->prepare("INSERT INTO users(username, password, email, role) VALUES(:username, :password, :email, :role);");
			$stmt->bindParam(':username', $username);
			$stmt->bindParam(':password', md5($password));
			$stmt->bindParam(':email', $email);
			$stmt->bindParam(':role', $role);
			$stmt->execute();

			header('Location: list_user.php');
        }
    }

?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add User</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form role="form" action="" method="POST">
                <div class="form-group">
                    <label for="username">Username</label>
                    <input class="form-control" type="text" name="username" id="username" value="<?php if (isset($username)) echo $username; ?>" />
                    <?php if (isset($error['username'])) echo $error['username'] ;?>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input class="form-control" type="password" name="password" id="password">
                    <?php if (isset($error['password'])) echo $error['password'] ;?>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input class="form-control" type="text" name="email" id="email" value="<?php if (isset($email)) echo $email; ?>">
                    <?php if (isset($error['email'])) echo $error['email'] ;?>
                </div>
                <div class="form-group">
                    <label for="role">Role</label>
                    <select class="form-control" id="role" name="role">
                        <option></option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                    </select>
                </div>
                <button type="submit" class="btn btn-success">Add</button>
                <a href="list_user.php" class="btn btn-danger">Back</a>
            </form>
        </div>
    </div>
</div>
<?php include "includes/footer.php"; ?>