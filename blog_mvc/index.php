<?php

$queryString = $_SERVER['QUERY_STRING'];

$controller = $_GET['controller'];
$action = $_GET['action'];

if ($controller && $action) {
	$controllerName = ucfirst($controller) . 'Controller';

	if (file_exists("controller/". $controllerName . '.php')) {
		include "controller/". $controllerName . '.php';

		$c = new $controllerName();

		if (method_exists($c, $action)) {
			$c->$action();
		} else {
			echo "<h1>Action Not Found</h1>";
		}

		exit();
	}

}

echo "<h1>404 Not Found</h1>";

