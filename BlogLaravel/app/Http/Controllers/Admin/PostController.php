<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\Post;
use Illuminate\Http\Request;
use Validator;

class PostController extends Controller
{
	protected $post;

	public function __construct(Post $post)
	{
		$this->post = $post;
	}

	public function index(Request $request)
	{
		return view('admin.post.index', [
			'posts' => $this->post->all()
		]);
    }

	public function add(Request $request)
	{
		if ($request->method() == 'POST') {
			$validator = Validator::make($request->all(), [
				'title' => 'required|unique:posts|max:255',
				'content' => 'required|max:1000',
				'category' => 'required|numeric',
				'tag' => 'required',
			]);

			if ($validator->fails()) {
				$errors = $validator->errors();
				$request->flash();

				return view('admin.post.add', [
					'errors' => $errors
				]);
			} else {
				$post = new Post();

				$post->title = $request->input('title');
				$post->content = $request->input('content');
				$post->category_id = $request->input('category');
				$post->tag_id = $request->input('tag');
				$post->user_id = 1;

				$post->save();

				return redirect()->route('listPost');
			}
		}

		return view('admin.post.add');
    }

	public function update(Request $request, $id = null)
	{
		$post = Post::find($id);
		return view('admin.post.update', ['post' => $post]);
    }

	public function delete(Request $request, $id)
	{
		try {
			Post::destroy($id);
			return redirect()->route('listPost');
		} catch (\Exception $exception) {
			var_dump($exception);
		}
    }
}
