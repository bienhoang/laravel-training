<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Model\User;

class UserController extends Controller
{
	protected $userModel;

	public function __construct(User $userModel)
	{
		$this->userModel = $userModel;
	}

	public function index()
	{
		return view('admin.user.list', ['users' => $this->userModel->getAll()]);
    }
}
