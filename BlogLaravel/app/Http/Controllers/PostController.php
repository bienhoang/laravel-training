<?php

namespace App\Http\Controllers;

use App\Model\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
	public function index()
	{
		$posts = Post::all();

		return view('pages.index', ['posts' => $posts]);
    }

	public function detail(Request $request, $id = null)
	{
		$post = Post::find($id);
		return view('pages.detail', ['post' => $post]);
    }
}
