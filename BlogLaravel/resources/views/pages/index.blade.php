@extends('layouts.master')
@section('content')
    <div id="content-wrap">

        <div class="row">

            <div id="main" class="eight columns">
                @foreach($posts as $post)
                <article class="entry">

                    <header class="entry-header">

                        <h2 class="entry-title">
                            <a href="{{route('detailPost', ['id' => $post->id])}}" title="">{{$post->title}}</a>
                        </h2>

                        <div class="entry-meta">
                            <ul>
                                <li>{{$post->created_at}}</li>
                                <span class="meta-sep">&bull;</span>
                                <li><a href="#" title="" rel="category tag">{{$post->category_id}}</a></li>
                                <span class="meta-sep">&bull;</span>
                                <li>John Doe</li>
                            </ul>
                        </div>

                    </header>

                    <div class="entry-content">
                        {!! $post->content !!}
                    </div>

                </article> <!-- end entry -->
                @endforeach
            </div> <!-- end main -->

            @include ('layouts.sidebar');

        </div> <!-- end row -->

    </div>
@endsection