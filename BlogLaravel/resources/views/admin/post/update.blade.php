@extends("admin.layouts.master")
@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Update Post</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <form role="form" action="" method="POST">
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input class="form-control" type="text" name="title" id="title" value="{{$post->title}}" />
                    </div>
                    <div class="form-group">
                        <label for="content">Content</label>
                        <textarea class="form-control" rows="3" name="content" id="content" >{{$post->content}}</textarea>
                    </div>
                    <div class="form-group">
                        <label for="category">Category</label>
                        <select class="form-control" id="category" name="category">
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tags</label>
                        <input class="form-control" type="text" placeholder="Nhập cách nhau bằng dấu phẩy" name="tag" id="tag"
                               value="" />
                    </div>
                    <button type="submit" class="btn btn-success">Update</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                </form>
            </div>
        </div>
    </div>
@endsection