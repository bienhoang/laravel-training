@extends('admin.layouts.master')
@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Add Post</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <form role="form" action="{{route('addPost')}}" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input class="form-control" type="text" name="title" id="title" value="{{old('title')}}">
                        @if (!empty($errors->get('title')))
                            @foreach($errors->get('title') as $error)
                                <div class="alert alert-danger">
                                    <ul>
                                        <li>{{ $error }}</li>
                                    </ul>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="content">Content</label>
                        <textarea class="form-control" rows="3" name="content" id="content" rows="10">{{old('content')}}</textarea>
                        @if (!empty($errors->get('content')))
                            @foreach($errors->get('content') as $error)
                                <div class="alert alert-danger">
                                    <ul>
                                        <li>{{ $error }}</li>
                                    </ul>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="category">Category</label>
                        <select class="form-control" id="category" name="category">
                            <option value=""></option>
                            <option value="1">1</option>
                            <option value="2">2</option>
                        </select>
                        @if (!empty($errors->get('category')))
                            @foreach($errors->get('category') as $error)
                                <div class="alert alert-danger">
                                    <ul>
                                        <li>{{ $error }}</li>
                                    </ul>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="form-group">
                        <label>Tags</label>
                        <input class="form-control" type="text" placeholder="Nhập cách nhau bằng dấu phẩy" name="tag" id="tag" value="{{old('tag')}}">
                        @if (!empty($errors->get('tag')))
                            @foreach($errors->get('tag') as $error)
                                <div class="alert alert-danger">
                                    <ul>
                                        <li>{{ $error }}</li>
                                    </ul>
                                </div>
                            @endforeach
                        @endif
                    </div>
                    <button type="submit" class="btn btn-success">Add</button>
                    <button type="reset" class="btn btn-danger">Reset</button>
                </form>
            </div>
        </div>
    </div>
@endsection