@extends("admin.layouts.master")
@section('content')
    <div id="page-wrapper">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">List Posts</h1>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Blog Post
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr>
                                    <th width="5%">#</th>
                                    <th width="25%">Title</th>
                                    <th width="25%">Summary</th>
                                    <th width="10%">Category</th>
                                    <th width="15%">Tag</th>
                                    <th width="10%">User</th>
                                    <th width="5%">Edit</th>
                                    <th width="5%">Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($posts as $post)
                                <tr>
                                    <td>{{$post->id}}</td>
                                    <td>{{$post->title}}</td>
                                    <td>{{$post->content}}</td>
                                    <td>{{$post->category_id}}</td>
                                    <td>
                                        <a href=""><span class="btn btn-outline btn-info">{{$post->tag_id}}</span></a>
                                    </td>
                                    <td>{{$post->user_id}}</td>
                                    <td><a href="{{route('updatePost', ['id' => $post->id])}}" class="btn btn-primary">Edit</a></td>
                                    <td><a href="{{route('deletePost', ['id' => $post->id])}}" class="btn btn-danger">Delete</a></td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <a href="{{route('addPost')}}" class="btn btn-warning">Add Post</a>
        </div>
    </div>
@endsection