<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('admin')->group(function () {
	Route::get('/', 'Admin\HomeController@index');
	Route::get('/user', 'Admin\UserController@index')->name('listAdminUser');
	Route::get('/user/edit', 'Admin\UserController@update')->name('editAdminUser');
	Route::get('/user/delete', 'Admin\UserController@delete')->name('deleteAdminUser');

	Route::get('/post', 'Admin\PostController@index')->name('listPost');

	Route::any('/post/add', 'Admin\PostController@add')->name('addPost');

	Route::get('/post/update/{id}', 'Admin\PostController@update')->name('updatePost');
	Route::get('/post/delete/{id}', 'Admin\PostController@delete')->name('deletePost');
});

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/post', 'PostController@index');
Route::get('/post/{id}', 'PostController@detail')->name('detailPost');
