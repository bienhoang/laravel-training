<?php


// Khai bao mang
$a = [1, 2, 3, 4, 5];
$b = [
	'name' => 'bien',
	'age' => '24'
];

$c = array('a', 'b', 'c');

// Truy cap phan tu cua mang
echo $a[0] . '<br/ >';
echo $b['name'] . "<br>";

foreach ($a as $value) {
	echo $value . '<br />';
}

foreach ($b as $key => $value) {
	echo "Key: " . $key . " - Value: " . $value . '<br/>';
}

$a[] = 6;
var_dump($a);

array_push($a, 7);

unset($a[1]);

$array = ['one', 'two', 'three'];

list($a, $b, $c) = $array;
echo $a . "<br>"; // one
echo $b . "<br>"; // two
echo $c; //three



$a = '1, 2, 3, 4, 5';
$b = '1. 2. 3. 4. 5. 6. 7';
$a = explode(',', $a);
$b = explode('.', $b);
var_dump($b + $a);
