<?php
ob_start();
session_start();
?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Login</title>
</head>
<body>
<?php
	$user = 'bienhoang';
	$password = '123456';
	$message = '';

	if ($_SERVER['REQUEST_METHOD'] == 'POST') {
		if ($_POST['username'] == $user && $_POST['password'] == $password) {
			$_SESSION['user'] = ['username' => $user];
			header('Location: index.php');
		} else {
			$message = 'Username or password wrong';
		}
	}

	if (!empty($_SESSION['user'])) {
		header('Location: index.php');
    }
?>
<form action="" method="POST">
	<label for="username">Username</label>
	<input type="text" placeholder="Username" name="username" />
	<label for="username">Password</label>
	<input type="password" placeholder="Password" name="password" />
	<input type="submit" value="Login" />
	<span>
		<?php if (!empty($message)) echo $message; ?>
	</span>
</form>
</body>
</html>