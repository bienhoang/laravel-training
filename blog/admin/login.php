<?php
include "includes/header.php";

$db = include "db.php";

if (isset($_SESSION['admin'])) {
	header('Location: index.php');
}

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $username = $_POST['username'];
    $password = $_POST['password'];

    if (empty($username)) {
		$error['username'] = 'Username is required';
	}

	if (empty($password)) {
		$error['password'] = 'Password is required';
	}

   if (empty($error)) {
	   $stmt = $db->prepare("SELECT id, username, email, role, password FROM users WHERE username=:username");
	   $db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
	   $stmt->bindParam(':username', $username);
	   $stmt->execute();

	   $result = $stmt->fetch(PDO::FETCH_ASSOC);

	   if ($result && $result['password'] == md5($password)) {
	       $_SESSION['admin'] = $result;
	       header('Location: index.php');
       } else {
	       $error['login'] = "Username or password not math";
       }
   }
}
?>
<div class="container">
	<div class="row">
		<div class="col-md-4 col-md-offset-4">
			<div class="login-panel panel panel-default">
				<div class="panel-heading">
					<h3 class="panel-title">Please Sign In</h3>
				</div>
				<div class="panel-body">
					<form role="form" action="" method="POST">
						<fieldset>
                            <span><?php if (isset($error['login'])) echo $error['login'] ;?></span>
							<div class="form-group">
								<input class="form-control" placeholder="Username" name="username" type="text" autofocus>
								<?php if (isset($error['username'])) echo $error['username'] ;?>
							</div>
							<div class="form-group">
								<input class="form-control" placeholder="Password" name="password" type="password" value="">
								<?php if (isset($error['password'])) echo $error['password'] ;?>
							</div>
							<div class="checkbox">
								<label>
									<input name="remember" type="checkbox" value="Remember Me">Remember Me
								</label>
							</div>
							<!-- Change this to a button or input when using this as a form -->
							<button type="submit" class="btn btn-lg btn-success btn-block">Login</button>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include "includes/footer.php"; ?>