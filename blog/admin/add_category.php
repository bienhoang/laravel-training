<?php
include "includes/header.php";
include "includes/sidebar.php";

if (!isset($_SESSION['admin'])) {
	header('Location: login.php');
	exit;
}

$db = include "db.php";

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
    $catName = $_POST['cat-name'];

    if (empty($catName)) {
        $error['catName'] = 'Category name is required';
    }

    if (empty($error)) {
        $stmt = $db->prepare("INSERT INTO categories(`name`) VALUES (:name)");
		$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
        $stmt->bindParam(':name', $catName);
        $stmt->execute();

        header('Location: list_category.php');
    }
}
?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add Category</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <form role="form" action="" method="POST">
                <div class="form-group">
                    <label for="cat-name">Name</label>
                    <input class="form-control" type="text" name="cat-name" id="cat-name">
					<?php if (isset($error['catName'])) echo $error['catName'] ;?>
                </div>
                <button type="submit" class="btn btn-success">Add</button>
                <button type="reset" class="btn btn-danger">Reset</button>
            </form>
        </div>
    </div>
</div>
<?php include "includes/footer.php"; ?>

