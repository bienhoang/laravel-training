<?php
include "includes/header.php";
include "includes/sidebar.php";
if (!isset($_SESSION['admin'])) {
	header('Location: login.php');
	exit;
}
$db = include "db.php";

$stmt = $db->prepare("SELECT * FROM users;");
$stmt->execute();

// Lấy ra danh sách User từ DB sử dụng FETCH_ASSOC -> sẽ trả lại dạng mảng User.
$listUser = $stmt->fetchAll(PDO::FETCH_ASSOC);

if (isset($_GET['delete'])) {
    $stmt = $db->prepare("DELETE FROM users WHERE id=:id");
    $stmt->bindParam(':id', $_GET['delete']);
    $stmt->execute();

    header('Location: list_user.php');
}

?>
<div id="page-wrapper">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">List User</h1>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    Blog User
                </div>
                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th width="5%">#</th>
                                <th width="25%">Username</th>
                                <th width="25%">Email</th>
                                <th width="10%">Role</th>
                                <th width="5%">Edit</th>
                                <th width="5%">Delete</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($listUser as $user) :?>
                            <tr>
                                <td><?=$user['id']; ?></td>
                                <td><?=$user['username']; ?></td>
                                <td><?=$user['email']; ?></td>
                                <td><?php if ($user['role'] == 1) echo 'Admin'; else echo 'Editor'; ?></td>
                                <td><a href="update_user.php?id=<?=$user['id'];?>" class="btn btn-primary">Edit</a></td>
                                <td><a href="list_user.php?delete=<?=$user['id'];?>" class="btn btn-danger">Delete</a></td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <a href="add_user.php" class="btn btn-warning">Add User</a>
    </div>
</div>
<?php include "includes/footer.php"?>