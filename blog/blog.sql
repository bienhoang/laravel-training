-- MySQL dump 10.13  Distrib 5.7.17, for macos10.12 (x86_64)
--
-- Host: 127.0.0.1    Database: blog
-- ------------------------------------------------------
-- Server version	5.6.41

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (5,'Laravel','2018-10-20 11:49:47','2018-10-20 11:49:47'),(6,'Symfony','2018-10-20 11:49:53','2018-10-20 11:49:53'),(7,'Zend Framework','2018-10-20 11:50:04','2018-10-20 11:50:04');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `post_id` int(10) NOT NULL,
  `content` text,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `website` varchar(100) DEFAULT NULL,
  `approved` tinyint(4) DEFAULT '0',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `posts`
--

DROP TABLE IF EXISTS `posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `content` text,
  `user_id` int(10) NOT NULL,
  `category_id` int(10) DEFAULT NULL,
  `tag_id` varchar(100) DEFAULT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `posts`
--

LOCK TABLES `posts` WRITE;
/*!40000 ALTER TABLE `posts` DISABLE KEYS */;
INSERT INTO `posts` VALUES (3,'Kinh táº¿ Trung Quá»‘c liÃªn tiáº¿p chá»‹u Ä‘Ã²n.','Chá»©ng khoÃ¡n cháº¡m Ä‘Ã¡y, tÄƒng trÆ°á»Ÿng tháº¥p nháº¥t gáº§n má»™t tháº­p ká»· Ä‘ang táº¡o thÃªm gÃ¡nh náº·ng cho lÃ£nh Ä‘áº¡o Trung Quá»‘c',9,7,'a:5:{i:0;s:1:\"1\";i:1;s:1:\"6\";i:2;s:2:\"24\";i:3;s:2:\"25\";i:4;s:2:\"26\";}','2018-10-20 13:57:40','2018-10-20 13:57:40');
/*!40000 ALTER TABLE `posts` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tags`
--

DROP TABLE IF EXISTS `tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tags`
--

LOCK TABLES `tags` WRITE;
/*!40000 ALTER TABLE `tags` DISABLE KEYS */;
INSERT INTO `tags` VALUES (1,'laravel','2018-10-20 13:17:06','2018-10-20 13:17:06'),(2,' bien','2018-10-20 13:17:06','2018-10-20 13:17:06'),(3,' hoang','2018-10-20 13:17:06','2018-10-20 13:17:06'),(4,' bien','2018-10-20 13:30:03','2018-10-20 13:30:03'),(5,' hoang','2018-10-20 13:30:03','2018-10-20 13:30:03'),(6,'bien','2018-10-20 13:57:40','2018-10-20 13:57:40'),(7,' hoang','2018-10-20 13:57:40','2018-10-20 13:57:40'),(8,' giang','2018-10-20 13:57:40','2018-10-20 13:57:40'),(9,' hoang','2018-10-20 14:00:42','2018-10-20 14:00:42'),(10,' giang','2018-10-20 14:00:42','2018-10-20 14:00:42'),(11,' hoang','2018-10-20 14:01:22','2018-10-20 14:01:22'),(12,' giang','2018-10-20 14:01:22','2018-10-20 14:01:22'),(13,' hoang','2018-10-20 14:01:35','2018-10-20 14:01:35'),(14,' giang','2018-10-20 14:01:35','2018-10-20 14:01:35'),(15,' hoang','2018-10-20 14:01:44','2018-10-20 14:01:44'),(16,' giang','2018-10-20 14:01:44','2018-10-20 14:01:44'),(17,' hoang','2018-10-20 14:01:48','2018-10-20 14:01:48'),(18,' giang','2018-10-20 14:01:48','2018-10-20 14:01:48'),(19,' hoang','2018-10-20 14:01:58','2018-10-20 14:01:58'),(20,' giang','2018-10-20 14:01:58','2018-10-20 14:01:58'),(21,' hoang','2018-10-20 14:02:13','2018-10-20 14:02:13'),(22,' giang','2018-10-20 14:02:13','2018-10-20 14:02:13'),(23,' zend','2018-10-20 14:02:13','2018-10-20 14:02:13'),(24,' hoang','2018-10-20 14:02:20','2018-10-20 14:02:20'),(25,' giang','2018-10-20 14:02:20','2018-10-20 14:02:20'),(26,' zend','2018-10-20 14:02:20','2018-10-20 14:02:20');
/*!40000 ALTER TABLE `tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `role` int(11) DEFAULT '2',
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (11,'admin','25d55ad283aa400af464c76d713c07ad','bien@techmaster.vn',1,'2018-10-20 14:05:08','2018-10-20 14:05:08');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-10-20 21:06:18
